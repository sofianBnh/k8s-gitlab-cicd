# Kubernetes/GitLab CI/CD Pipeline for Java

> Sofiane Benahmed


## Overview

In this project, the implementation of a full CI/CD pipeline is presented as well as how to deploy its infrastructure. The deployment includes a Kubernetes cluster and a gitlab server. As for the pipeline, it is made for a java spring boot application. The pipeline uses Gradle for automating the tests and the build, and gitlab is used for the deployment on k8s.

## CI/CD Stages

CI/CD pipelines where made to optimize the workflow of development teams to facilitate the test packing and deployment of their latest projects. To achieve this a pipeline usually englobes the following stages:

1. **Code and Versioning**: In this stage, the project is under development and it is maintained using some kind of version control. The final step in this stage is a `commit` which means the upload of the developer’s code to the version control platform. This commit can be either to a development (feature) branch or to the `master` branch which is the main one where the stable parts of the code are accumulated.
2. **Build**: Once a commit is received, the pipeline should build the project into some kind of deployable package. In our case (Java Application) the final package is in form of a `jar` file.  This procedure and final package differ from language to another hence require a specific procedure.
3. **Test**: Once the build packed, it is ready for an initial set of test, the Unit Tests. These tests are made to evaluate the functioning of the submodules of the project separately. Although in some cases these tests can be run before the build stage, it can be after as well.
4. **Deployment to Test Servers**: This phase is the end of the continuous integration and the beginning of the continuous delivery. In this stage, the tested build is deployed to test server for further tests (ex. from the QA Team or Beta Testers)
5. **Auto Tests and QA**: This phase is the last testing phase before the deployment, Here the project is tested as whole to check the general integration of the components with each other and with the environment. This environment not only includes the servers but also the difference APIs and projects linked to this project. 
6. **Deployment to Production**: Finally once the project has been tested from all the above stages and approved, it is confidently deployed to production servers.

What’s important to note about these pipelines is the feedback after each stage. In case of the failure (and sometimes even success) the pipeline’s jobs are to transfer the feedback of the failure to the development and Operations Teams often in the shape of `Issues` which will need to be fixed.

------

## Project Description

The project that we will be deploying in this lab is a simple back-end Web Server with only health check endpoint and a greeting endpoint. It has the following characteristics:

- Type: Web Back end Application
- Language: Java
- Java Development Kit Version: 8u191
- Framework: Spring Boot
- Unit Testing Framework: Spring Unit Tests
- Deployment Archive: Jar
- Dependency Manager and Build Automation System: Gradle
- Web Server: Integrated Tomcat
- Version Control Platform: Local GitLab Server
- Containerisation Platform: Docker
- Deployment Platform: Local Kubernetes Cluster

------

## Infrastructure

To have a better experience with the tools used in this lab, we decided to deploy them locally. These tools include a Kubernetes Cluster and GitLab server. The architecture looks as follows:

![infra](img/infra.png)

### Kubernetes Setup

We start by setting up Kubernetes. These steps are concatenated, for a full version of the installation refer to [k8s-deployment](https://bitbucket.org/sofianBnh/k8s-on-prem) since it is a similar setup. We start by installing `docker.io`, `kubectl` and `kubeadm`. Then one the master node we configure the Kubernetes network and the IPs and external IP. These parameters are what allows us to connect our versioning server to the Kubernetes API:

```bash
# Reset the old configuration
kubeadm reset
rm -rf ~/.kube

# Initialize the cluster
kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=0.0.0.0 --apiserver-cert-extra-sans=192.168.122.2

# Create the Kubeconfig of the currnet user
mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config

# Setup the network (flannel)
kubectl apply -f "https://raw.githubusercontent.com/coreos/flannel/bc79dd1505b0c8681ece4de4c0d86c5cd2643275/Documentation/kube-flannel.yml"
```

This will propmt us with a command to add a slave server to our cluster. We apply it to the other node (slave1) after installing the same packages as for the master:

```bash
kubeadm join 192.168.122.2:6443 --token n16k5c.0puvu6yiw0b87wgn --discovery-token-ca-cert-hash sha256:aa6115a3d64554c1462a8eaef69e458a931daaedcf9de95c46a6cd5e4d09b27c
```

Then we install the Kubernetes Dashboard:

```bash
kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/master/aio/deploy/recommended/kubernetes-dashboard.yaml

kubectl -n kube-system edit service kubernetes-dashboard
# Setting the spec: type: ClusterIP to NodePort

# Create an Admin Service Account
kubectl apply -f manifest/create-admin.yaml
kubectl -n kube-system describe secret \
 $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
```

Result:

```shell
root@master# kubectl get nodes 
```

![nodes](img/nodes.png)

![dash](img/dash.png)

The `yaml` files can be found under *files/manifest*. 

### GitLab Setup

Next, we set up the GitLab server, to do so we can use the official docker image via command line:

```bash
docker pull gitlab/gitlab-ce
docker run --detach \
  --hostname gitlab.example.com \
  --publish 443:443 --publish 80:80 --publish 22:22 \
  --name gitlab \
  --restart always \
  --volume /srv/gitlab/config:/etc/gitlab \
  --volume /srv/gitlab/logs:/var/log/gitlab \
  --volume /srv/gitlab/data:/var/opt/gitlab \
  gitlab/gitlab-ce:latest
```

Or docker-compose:

```yaml
version: "3"
services:
     web:
       image: 'gitlab/gitlab-ce:latest'
       restart: always
       container_name: gitlab
       hostname: '192.168.122.4' # Important Parameter: Resolves Naming issues
       ports:
         - '80:80'
         - '443:443'
         - '22:22'
       volumes:
         - '/srv/gitlab/config:/etc/gitlab'
         - '/srv/gitlab/logs:/var/log/gitlab'
         - '/srv/gitlab/data:/var/opt/gitlab'
       network_mode: host # Important Parameter: Allows access to Kubernetes
```

We check the logs and wait for the end of the  installation:

```bash
docker logs -f gitlab
```

![up-logs](img/up-logs.png)

Then we can access the web interface:

![gitlab-up](img/gitlab-up.png)

### Kubernetes with GitLab 

Finally, to prepare the integration of Kubernetes with GitLab, we create a special user for GitLab and give is the cluster admin role for it to be able to deploy our deployment and services:

```bash
kubectl apply -f gitlab-admin-service-account.yaml 
kubectl apply -f gitlab-admin-cluster-role-binding.yaml 
```

The `yaml` files can be found under *files/manifest*. 

------

## Continuous Integration

1)- **Creating the repository:** we start by creating a repository on the GitLab page:

![repo-create](img/repo-create.png)

2)- **Creating the project:** Then we create our simple Spring Boot Project which includes the following files:

```bash
.
├── build.gradle    # Gradle Build Configuration
├── Dockerfile
├── gradle          # Gradle Wrapper for offline builds
├── gradlew         # Gradle Linux Building Script
├── gradlew.bat     # Gradle Windows Building Script
├── settings.gradle # Global Gradle Settings
└── src             # Source Code
    ├── main        # Web Server Code
    └── test        # Unit Tests / Integration Tests Code
```

What concerns us in this lab are the following parts:

- Unit tests:

  ```java
  @RunWith(SpringRunner.class)
  @SpringBootTest
  public class RestBackendApplicationTests {
      @Test
      public void contextLoads() {
          /* Tests to load the whole application and
           * checks for misconfigured components 
           */
      }
  }
  ```

- Gradle Configuration:

  ```json
  plugins {
      id 'org.springframework.boot' version '2.1.3.RELEASE'
      id 'java'
  }
  
  // Pulls the dependencies and packs them with the jar
  apply plugin: 'io.spring.dependency-management'
  
  group = 'su.os3.std1'
  version = '0.0.1-SNAPSHOT' // Could be an environment variable
  sourceCompatibility = '1.8' // Java version for the Build 
  
  // Libraries source
  repositories {
      mavenCentral()
  }
  
  dependencies {
      // library for unit testing Spring Applications
      testImplementation 'org.springframework.boot:spring-boot-starter-test'
      ...
  }
  ```

- Dockerfile:

  ```dockerfile
  FROM openjdk:8-jdk-alpine
  ...
  # Gets the result of the gradle build
  ADD /build/libs/rest-backend-0.0.1-SNAPSHOT.jar app.jar
  ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
  ```

  

3)- Adding to the repository:

```bash
cd rest-backend
git init
git remote add origin git@192.168.122.5:root/rest-backend.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

![pushed](img/pushed.png)

4)-  Connecting Kubernetes for Runners: To have the application which will run our pipeline, we connect our Kubernetes Cluster to this repository. First, we go to the appropriate section (Repository > Operations > Kubernetes):

![repo-kube](img/repo-kube.png) 

We select `Add Kubernetes Cluster` and Select `Add Existing Cluster`, then we fill it with the credentials from the Kubernetes Secret generated with the GitLab Account:

![secret](img/secret.png)

> Select Kubernetes Secrets

![kube-git-conf](img/kube-git-conf.png)

> Showing the Credentials

![filling-kube-conf](img/filling-kube-conf.png)

> Filling the Credential Data in GitLab

Once the cluster connected to GitLab, we are able to install the extensions needed such as `Helm`and `GitLab Runner`:

![install-apps](img/install-apps.png)

> Installing Runner after having Installed Helm

Then we can see GitLab already created a Runner (Settings > CI \ CD > Expand Runners):

![runner](img/runner.png)

Finally, since we are planning to add Docker packaging and delivery to DockerHub, we add our credentials as K8S secrets via GitLab’s Environment Variables (Settings > CI \ CD > Expand Environment variables):

![secret_vars](img/secret_vars.png)

Such as**:**

- **K8S_SECRET_CI_REGISTRY**: The register, in our case docker hub: *docker.io*
- **K8S_SECRET_CI_REGISTRY_IMAGE**: Name of the image from dockerhub.
- **K8S_SECRET_CI_REGISTRY_USER**: Our username for dockerhub.
- **K8S_SECRET_CI_REGISTRY_PASSWORD**: Our password.

And we create the Docker Hub repository on the website.

4)- Now that we have a Runner, we can start configuring our pipeline. This pipeline will cover for now the following stages:

- Build: This will call the Gradle script with the argument `assemble` to create the Jar then collect it
- Test: This will call the Gradle script with the argument `check` to run the Unit Tests
- Package: This will Build the Docker image from our application 

a)- We start by specifying our environment:

```yaml
image: docker:stable

services:
  - docker:dind

stages:
  - build
  - test
  - package
```

**Description:** Here we define the docker version that we want the service. The service is the main docker image that the stages will be run on. In our case we used Docker-in-Docker. This will allow us to use different images in our stages if we desire to. In a real-world scenario, this would be useful for testing the application on multiple versions of Java for example. In that case, we would only need to specify two different images. After that, we define our stages which will be linked to our executions. 

b)- Then we configure the build:

```yaml
gradle-build:
  stage: build
  image: openjdk:8-jdk-alpine
  script:
    - ./gradlew assemble
  artifacts:
    paths:
      - build/libs/*.jar
    expire_in: 1 weeks
```

**Description:**  In this part, we build the project and collect the artifacts from the build directory.

c)- And the Test:

```yaml
test:
  stage: test
  image: openjdk:8-jdk-alpine
  script:
    - ./gradlew test
```

**Description:**  Here we start the tests on Java 8.

d)- And Finally the packaging:

```yaml
docker-build:
  stage: package
  script:
    - export DOCKER_HOST="tcp://localhost:2375" # We need to specify it for K8s
    - docker build -t "$K8S_SECRET_CI_REGISTRY_IMAGE" .
    - docker login -u "$K8S_SECRET_CI_REGISTRY_USER" -p "$K8S_SECRET_CI_REGISTRY_PASSWORD" $K8S_SECRET_CI_REGISTRY
    - docker login
    - docker push "$K8S_SECRET_CI_REGISTRY_IMAGE"
```

**Description:** Finally, here we build the image and push it to docker hub. We can notice the usage of the previously defined variables.

5)-  Now that our pipeline is ready we commit it with our files under the name `.gitlab-ci` and GitLab will run it automatically. At the end of the execution of all the steps we get:

![passed](img/passed.png)

And we get notified of the deployed Docker Image in the email associated with Docker hub.

Finally, we can test it:

![run](img/run.png)

------

## Continuous Delivery

0)- To allow a remote Deployment on our Kubernetes Cluster, we needed to create a user with the right privileges as we did while creating the `gitlab` user. Then we prepare a `kubeconfig` by copying the structure from the local structure in `~/.kube/config` and filling the data from the new account, for more [details](http://docs.shippable.com/deploy/tutorial/create-kubeconfig-for-self-hosted-kubernetes-cluster/). To create this configuration file, we start with the following template:

```yaml
apiVersion: v1
kind: Config
users:
- name: <username>
  user:
    token: <token info>
clusters:
- cluster:
    certificate-authority-data: <certificate-authority-data info>
    server: <server IP and port>
  name: <cluster name>
contexts:
- context:
    cluster: <cluster name>
    user: <username>
  name: <usernmae>@<cluster name>
current-context: <usernmae>@<cluster name>
```

To get these information we use the following commands:

For the Cluster Certificate and the server info:

```bash
kubectl config view --flatten --minify > cluster-cert.txt
```

For the user token:

```bash
kubectl -n kube-system describe secret \
 $(kubectl -n kube-system get secret | grep <usernmae> | awk '{print $1}')
```

And we base64 using:

```bash
base64 <config file>
```

Once this is done, we base64 encode this and add it to the environment variables:

![base64_config_var](img/base64_config_var.png)

1)- Update the Pipeline: To add the Continuous Delivery, we needed to add another stage. In this stage we only have one job `deply_dev`: 

```yaml
variables:
  KUBECONFIG: ~/.kube/config
...
stages:
  - ..
  - deploy
...
deploy_dev:
  stage: deploy
  image: alpine
  environment:
    name: dev
  script:
    - mkdir ~/.kube/
    - apk add --no-cache curl
    - curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
    - chmod +x ./kubectl
    - mv ./kubectl /usr/local/bin/kubectl
    - echo $K8S_SECRET_KUBE_CONFIG | base64 -d > ~/.kube/config
    - export  KUBECONFIG=~/.kube/config
    - kubectl apply -f /root/rest-backend/deployment.yaml
```

**Description**: In this part, we pull an Alpine Image and setup `kubectl` to connect to our Kubernetes Cluster, then we copy a context (Credentials) to `~/.kube/config` by base64 decoding the variable that we set previously.  Then we set the environment variable to indicate to `kubectl` where to look for credentials. Finally, we apply our deployment. This procedure was made to avoid using the native support which requires an already set up the `DNS` and `TLS` Certificates. 

This deployment is a simple Kubernetes Deployment with a Pod containing our `rest-backend` container. The configuration for it can be seen below (the full deployment with Ingress and Load Balancing can be found in the *files/project/rest-backend/deployment.yaml*):

```yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: deploy-rest-backend
  labels:
    app: rest-backend
spec:
  selector:
    matchLabels:
      app: rest-backend
  template:
    metadata:
      labels:
        app: rest-backend
    spec:
      containers:
      - name: rest-backend
        imagePullPollicy: Always
        image: sofianerb/rest-backend:v1
        ports:
        - containerPort: 8080
```

2)-Then we push the new files and see the pipeline execute:

![deploy_w_ingress](img/deploy_w_ingress.png)

And we check Kubernetes:

![deploy_w_ingress_k](img/deploy_w_ingress_k.png)

And we follow the link:

![up_on_k](img/up_on_k.png)

------

## Final Test

Here we will demonstrate the full pipeline. With the same project, we push a simple modification, a new endpoint:

```java
@RestController
@RequestMapping("doors")
public class TestController {
    @GetMapping("/mantra")
    public String getDoorsRule() {
        return "All the locks can be opened :) ...just ask Ivan";
    }
}
```

Then we push:

```bash
git add .
git commit -m "Add Locks Endpoint"
git push
```

We can see the pipeline has been triggered (old commit):

![fina_pipeline_triggered](img/fina_pipeline_triggered.png)

And we can see the stages:

- **Build:**

  ![build_passed](img/build_passed.png)

- **Tests:**

  ![test_passed](img/test_passed.png)

- **Packaging:**

  ![package_passed](img/package_passed.png)

- **Deployment:**

  ![package_passed](img/deploy_passed.png)

And we can see our deployment in Kubernetes:

![deploy_passed_k](img/deploy_passed_k.png)

And our newly created endpoint:

![endpoint](img/endpoint.png)



## Final Final Test

After having the hello world application running normaly using our pipeline, we decided to do more and have a bigger project on our pipline. This project has the same specifications as the hello world application. It’s a Java Spring application with Unit Test and the deployment should made made to our development Kubernetes Cluster. As an enhancment we add the following feature to our previoud pipeline:

1)- Pulling the test results of JUnit:

```yaml
test-java8:
  ...
  artifacts: 
      paths:
      - build/test-results/test/TEST-*.xml
    reports:
      junit: build/test-results/test/TEST-*.xml
```

2)- Adding a vulnerability check of our dependencies using OWASP Dependency Check:

```json
// In build.gradle
buildscript {
    ...
    dependencies {
        classpath('org.owasp:dependency-check-gradle:4.0.1')
        ...
    }
}
apply plugin: 'org.owasp.dependencycheck'
```

```yaml
vulnerability_test:
  stage: test
  allow_failure: true
  image: openjdk:8-jdk-alpine
  script:
    - ./gradlew dependencyCheckAnalyze
  artifacts:
    when: always
    expire_in: 30 day
    paths:
      - build/reports/dependency-check-report.html
```

These two options will allow the developers to check the reports of the unit tests and the vulnerability check once the pipeline passes them.

After that we followed the same procedure as the previous one with creating a repository, linking it to our Kubernetes Cluster and adding the appropriate secrets to the CI \ CD Environment Variables. After having done that, we push our code and our pipeline gets triggered and we can see the following results:

Here we can see an example of the tests not being run correctly:

![fail_example](img/fail_example.png)

Now we fix that issue and try again:

![passed_api_build](img/passed_api_build.png)

 ![passed_api_test](img/passed_api_test.png)

![passed_api_vuln](img/passed_api_vuln.png)

![passed_api_docker](img/passed_api_docker.png)

![passed_api_deploy](img/passed_api_deploy.png)

At the end, we see the full pipeline being executed correctly:

![passed_api_detail](img/passed_api_detail.png)


And we can see in Kubernetes:

![api_in_k](img/api_in_k.png)

![work2](img/work2.png)

![work](img/work.png)

------

## Sources

- https://docs.gitlab.com/ee/user/project/clusters/index.html#adding-an-existing-kubernetes-cluster
- http://docs.shippable.com/deploy/tutorial/create-kubeconfig-for-self-hosted-kubernetes-cluster/
- https://stackoverflow.com/questions/42867039/gitlab-ci-runner-cant-connect-to-unix-var-run-docker-sock-in-kubernetes
- https://about.gitlab.com/2017/09/21/how-to-create-ci-cd-pipeline-with-autodeploy-to-kubernetes-using-gitlab-and-helm/
- http://docs.shippable.com/deploy/tutorial/create-kubeconfig-for-self-hosted-kubernetes-cluster/
- https://medium.com/@brunose/gitlab-ci-cd-kubernetes-65eec29d0555
- https://dzone.com/articles/learn-how-to-setup-a-cicd-pipeline-from-scratch
- https://edenmal.moe/post/2018/GitLab-Kubernetes-Using-GitLab-CIs-Kubernetes-Cluster-feature/
- https://gist.github.com/daicham/5ac8461b8b49385244aa0977638c3420
- https://stackoverflow.com/questions/51805446/how-to-make-gitlab-ci-release-a-spring-boot-jar-using-gradle
- https://about.gitlab.com/2016/12/14/continuous-delivery-of-a-spring-boot-application-with-gitlab-ci-and-kubernetes/
- https://stackoverflow.com/questions/45517733/how-to-publish-docker-images-to-docker-hub-from-gitlab-ci
- https://kubernetes.io/docs/home/
- https://docs.gitlab.com/
- https://github.com/alex-oleshkevich/docker-registry